package paulinr.edh.ht.nfccartevisite.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import paulinr.edh.ht.nfccartevisite.Model.Carte;

/**
 * Created by Ing Paulinr on 03/03/2018.
 */
public class DbManager {
    public static final String DEBUG_TAG = "CarteManager";   // for debugging (to find in logs)

    // SQLite database details
    public static final int DB_VERSION = 4;
    public static final String DB_NAME = "Db_carte.db";


    // Creation de la table carte et  mes cartes
    private static final String CREATE_TABLE_CARDS = "CREATE TABLE cards (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            " nom TEXT NOT NULL, homePhone TEXT NOT NULL, businessPhone TEXT, email TEXT, compagnie TEXT," +
            "addresse TEXT, poste TEXT, facebook TEXT, skype TEXT, autres TEXT, profilePath TEXT);";
    private static final String CREATE_TABLE_MYCARDS = "CREATE TABLE mycards (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            " nom TEXT NOT NULL, homePhone TEXT NOT NULL, businessPhone TEXT, email TEXT, compagnie TEXT," +
            "addresse TEXT, poste TEXT, facebook TEXT, skype TEXT, autres TEXT, profilePath TEXT);";

    // DROP TABLE command (for CARDS and MYCARDS tables)
    public static final String DB_DROP_TABLE_CARDS = "DROP TABLE IF EXISTS cards";
    public static final String DB_DROP_TABLE_MYCARDS = "DROP TABLE IF EXISTS mycards";

    // SQLite
    private SQLiteDatabase db;
    private Context context;
    private DatabaseHelper dbHelper;

    // constructor
    public DbManager (Context context){
        this.context = context;}
         
    // open connection to database
    public DbManager open(){
        dbHelper = new DatabaseHelper(context,DB_NAME,null,DB_VERSION);
        try{
            db = dbHelper.getWritableDatabase();
            //Toast.makeText(context,"Connection to DB is open (read/write)",Toast.LENGTH_SHORT).show();
        }
        catch (SQLException e){
            db = dbHelper.getReadableDatabase();
            //Toast.makeText(context,"Connection to DB is open (read only)",Toast.LENGTH_SHORT).show();
        }
        return this;
    }

    // close connection to database
    public void close(){
        dbHelper.close();
        //Toast.makeText(context,"Connection to DB is closed",Toast.LENGTH_SHORT).show();
    }

    // inserer nouvelle carte dans la base de donnees
    public boolean insertNewCard(String table, Carte carte){
        ContentValues newCardValues = new ContentValues();

        newCardValues.put("nom",carte.getNom());
        newCardValues.put("homePhone",carte.getHomePhone());
        newCardValues.put("businessPhone",carte.getBusinessPhone());
        newCardValues.put("email",carte.getEmail());
        newCardValues.put("compagnie",carte.getCompagnie());
        newCardValues.put("addresse",carte.getAddresse());
        newCardValues.put("poste",carte.getPoste());
        newCardValues.put("facebook",carte.getFacebook());
        newCardValues.put("skype",carte.getSkype());
        newCardValues.put("autres",carte.getAutres());
        newCardValues.put("profilePath",carte.getImgPathLogo());

        return db.insert(table,null,newCardValues) > 0;
    }

    // Mise a jour des champs de  la carte
    public boolean updateCard(String table, Carte carte){
        ContentValues updateCardValues = new ContentValues();

        updateCardValues.put("nom",carte.getNom());
        updateCardValues.put("homePhone",carte.getHomePhone());
        updateCardValues.put("businessPhone",carte.getBusinessPhone());
        updateCardValues.put("email",carte.getEmail());
        updateCardValues.put("compagnie",carte.getCompagnie());
        updateCardValues.put("addresse",carte.getAddresse());
        updateCardValues.put("poste",carte.getPoste());
        updateCardValues.put("facebook",carte.getFacebook());
        updateCardValues.put("skype",carte.getSkype());
        updateCardValues.put("autres",carte.getAutres());
        updateCardValues.put("profilePath",carte.getImgPathLogo());



        return db.update(table,updateCardValues,"id = " + carte.getId(),null) > 0;
    }

    //Supprimer une carte dans la base de donnees
    public boolean deleteCard(String table, long id){

        return db.delete(table,"id = " + id,null) > 0;
    }

    // Lister toutes les cartes de la base de donnees
    public Cursor getAllCardsFromDB(String table){

        Cursor cardsCursor;
        String [] columns = {"id","nom","homePhone","businessPhone","email","compagnie","addresse","poste","facebook","skype","autres","profilePath"};
        cardsCursor = db.query(table,columns,null,null,null,null,null);
        return cardsCursor;
    }


    // private class DatabaseHelper - needed for creating and updating database tables
    private static class DatabaseHelper extends SQLiteOpenHelper {

        // constructor
        public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL(CREATE_TABLE_CARDS);
            sqLiteDatabase.execSQL(CREATE_TABLE_MYCARDS);
            Log.d(DEBUG_TAG,"Les tables carte et mesCartes ont été créées avec succès....");
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
            sqLiteDatabase.execSQL(DB_DROP_TABLE_CARDS);
            sqLiteDatabase.execSQL(DB_DROP_TABLE_MYCARDS);
            Log.d(DEBUG_TAG,"Les tables carte et mesCartes ont été supprimées.... Toutes les données sont perdues !");

            onCreate(sqLiteDatabase);
        }
    }



}
