package paulinr.edh.ht.nfccartevisite.Activity;

import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import paulinr.edh.ht.nfccartevisite.Model.Carte;
import paulinr.edh.ht.nfccartevisite.R;


/**
 * Created by Ing Paulinr on 03/03/2018.
 */
public class AfficherCarteActivity extends AppCompatActivity{

    private Carte maCarte;
    private Intent cardIntent;
    JSONObject cardJSON = new JSONObject();

    private long id;
    private ImageView profile;
    private TextView nom;
    private TextView homePhoneTxt;
    private TextView homePhone;
    private TextView businessPhoneTxt;
    private TextView businessPhone;
    private TextView emailTxt;
    private TextView email;
    private TextView compagnieTxt;
    private TextView compagnie;
    private TextView addresseTxt;
    private TextView addresse;
    private TextView posteTxt;
    private TextView poste;
    private TextView facebookTxt;
    private TextView facebook;
    private TextView skypeTxt;
    private TextView skype;
    private TextView autresTxt;
    private TextView autres;


    private Button shareBluetooth;
    // set how long device should be visible
    private static final int DISCOVER_DURATION = 300;
    private static final int REQUEST_BLU = 1;

    private NfcAdapter nfcAdapter;
    private PendingIntent nfcPendingIntent;
    private IntentFilter[] ndefIntentFilters;

    private boolean mWriteMode = false;
    private IntentFilter[] mWriteTagFilters;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_afficher_carte);

        profile = (ImageView)findViewById(R.id.imageView);
        nom = (TextView)findViewById(R.id.carte_nomVal);
        homePhoneTxt = (TextView)findViewById(R.id.carte_homePhoneTxt);
        homePhone = (TextView)findViewById(R.id.carte_homePhoneVal);
        businessPhoneTxt = (TextView)findViewById(R.id.carte_businessphoneTxt);
        businessPhone = (TextView)findViewById(R.id.carte_businessphoneVal);
        emailTxt = (TextView)findViewById(R.id.carte_emailTxt);
        email = (TextView)findViewById(R.id.carte_emailVal);
        compagnieTxt = (TextView)findViewById(R.id.carte_companieTxt);
        compagnie = (TextView)findViewById(R.id.carte_companieVal);
        addresseTxt = (TextView)findViewById(R.id.carte_addresseTxt);
        addresse = (TextView)findViewById(R.id.carte_addresseVal);
        posteTxt = (TextView)findViewById(R.id.carte_posteTxt);
        poste = (TextView)findViewById(R.id.carte_posteVal);
        facebookTxt = (TextView)findViewById(R.id.carte_facebookTxt);
        facebook = (TextView)findViewById(R.id.carte_facebookVal);
        skypeTxt = (TextView)findViewById(R.id.carte_skypeTxt);
        skype = (TextView)findViewById(R.id.carte_skypeVal);
        autresTxt = (TextView)findViewById(R.id.carte_autresTxt);
        autres = (TextView)findViewById(R.id.carte_autresVal);

        shareBluetooth = (Button)findViewById(R.id.bluetooth_btn);

        // Définir les valeurs de l'objet Carte prises à partir de l'intention (and hide empty fields)
           setMyCardValues();

        /**
         * Filtre NfcAdapter et Intent pour désactiver l'exécution d'une application en double lors de la réception d'un message Intent from NDEF
         */
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcPendingIntent = PendingIntent.getActivity(this,0,new Intent(this,getClass()).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);
        IntentFilter ndefFilter = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            ndefFilter.addDataType("text/plain");                   // définir le type de charge utile qui nous intéresse dans le message NDEF
            ndefIntentFilters = new IntentFilter[]{ndefFilter};
        }
        catch (IntentFilter.MalformedMimeTypeException e) {
            e.printStackTrace();
        }

        // Intent filters for writing to a tag
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        mWriteTagFilters = new IntentFilter[]{tagDetected};

    }
    // Supprimer ma  carte
    public void deleteMyCard(View view){
        new AlertDialog.Builder(this).setTitle("Supprimer la carte de visite?")
                .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String action = cardIntent.getStringExtra("action");
                        if (action.equals("maCarte")) cardIntent.setClass(getApplicationContext(), MesCartesActivity.class);
                        else if (action.equals("cardFromList")) cardIntent.setClass(getApplicationContext(),ListCartesActivity.class);
                        putCardInfoToIntent();
                        cardIntent.putExtra("action","delete");     // Card from this Intent should be deleted in database
                        cardIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(cardIntent);
                    }
                })
                .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        return;
                    }
                })
                .show();
    }
    public void editMyCard(View view){
        cardIntent.setClass(this, EditerCarteActivity.class);
        putCardInfoToIntent();
        startActivity(cardIntent);
    }

    // méthode privée pour définir toutes les valeurs de macarte prises à partir de l'intention
    private void setMyCardValues(){
        cardIntent = getIntent();
        //Si l'utilisateur a édité la carte ou créé une nouvelle carte, alors prenez les valeurs de l'intention
        maCarte = new Carte(cardIntent.getStringExtra("nom"), cardIntent.getStringExtra("homePhone"), cardIntent.getStringExtra("businessPhone"),
                cardIntent.getStringExtra("email"), cardIntent.getStringExtra("compagnie"), cardIntent.getStringExtra("addresse"), cardIntent.getStringExtra("poste"),
                cardIntent.getStringExtra("facebook"), cardIntent.getStringExtra("skype"),
                cardIntent.getStringExtra("autres"),cardIntent.getStringExtra("imgPathLogo"));
        maCarte.setId(cardIntent.getLongExtra("id",0));

        if (maCarte.getImgPathLogo() == null || maCarte.getImgPathLogo().equals("null")) profile.setImageResource(R.drawable.user_contact);
        else profile.setImageURI(Uri.parse(maCarte.getImgPathLogo()));
        nom.setText(maCarte.getNom());
        homePhone.setText(maCarte.getHomePhone());
        businessPhone.setText(maCarte.getBusinessPhone());
        email.setText(maCarte.getEmail());
        compagnie.setText(maCarte.getCompagnie());
        addresse.setText(maCarte.getAddresse());
        poste.setText(maCarte.getPoste());
        facebook.setText(maCarte.getFacebook());
        skype.setText(maCarte.getSkype());
        autres.setText(maCarte.getAutres());

        hideEmptyFields();          // masquer les champs vides
    }

    // méthode privée pour masquer les champs vides MyCard
    private void hideEmptyFields(){

        if (homePhone.length() == 0) {
            homePhoneTxt.setVisibility(View.GONE);
            homePhone.setVisibility(View.GONE);
        }
        if (businessPhone.length() == 0) {
            businessPhoneTxt.setVisibility(View.GONE);
            businessPhone.setVisibility(View.GONE);
        }
        if (email.length() == 0) {
            emailTxt.setVisibility(View.GONE);
            email.setVisibility(View.GONE);
        }
        if (compagnie.length() == 0) {
            compagnieTxt.setVisibility(View.GONE);
            compagnie.setVisibility(View.GONE);
        }
        if (addresse.length() == 0) {
            addresseTxt.setVisibility(View.GONE);
            addresse.setVisibility(View.GONE);
        }
        if (poste.length() == 0) {
            posteTxt.setVisibility(View.GONE);
            poste.setVisibility(View.GONE);
        }
        if (facebook.length() == 0) {
            facebookTxt.setVisibility(View.GONE);
            facebook.setVisibility(View.GONE);
        }

        if (skype.length() == 0) {
            skypeTxt.setVisibility(View.GONE);
            skype.setVisibility(View.GONE);
        }
        if (autres.length() == 0) {
            autresTxt.setVisibility(View.GONE);
            autres.setVisibility(View.GONE);
        }
    }

    // méthode privée permettant de placer les données de la carte (champs) dans l'objet Intent
    private void putCardInfoToIntent(){
        if (cardIntent.getStringExtra("action").equals("myCard")) {cardIntent.putExtra("action","editMyCard");}
        else if (cardIntent.getStringExtra("action").equals("cardFromList")) {cardIntent.putExtra("action","edit");}
        cardIntent.putExtra("id",maCarte.getId());
        cardIntent.putExtra("imgPathLogo",maCarte.getImgPathLogo());
        cardIntent.putExtra("nom",maCarte.getNom());
        cardIntent.putExtra("homePhone",maCarte.getHomePhone());
        cardIntent.putExtra("businessPhone",maCarte.getBusinessPhone());
        cardIntent.putExtra("email",maCarte.getEmail());
        cardIntent.putExtra("compagnie",maCarte.getCompagnie());
        cardIntent.putExtra("addresse",maCarte.getAddresse());
        cardIntent.putExtra("poste",maCarte.getPoste());
        cardIntent.putExtra("facebook",maCarte.getFacebook());
        cardIntent.putExtra("skype",maCarte.getSkype());
        cardIntent.putExtra("autres",maCarte.getAutres());
    }

    // lorsque le bouton "retour" est pressé, retourner à la bonne activité
    @Override
    public void onBackPressed(){
        cardIntent = getIntent().setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if(cardIntent.getStringExtra("action").equals("myCard") || cardIntent.getStringExtra("action").equals("editMyCard")||
                cardIntent.getStringExtra("action").equals("myCardWriteToTag")){
            cardIntent.setClass(this,MainActivity.class);
        }
        else if(cardIntent.getStringExtra("action").equals("cardFromList") || cardIntent.getStringExtra("action").equals("edit")
                || cardIntent.getStringExtra("action").equals("writeToTag")){
            cardIntent.setClass(this,ListCartesActivity.class);
        }
        else{
            Toast.makeText(getApplicationContext(),"BACK button not defined\naction="+cardIntent.getStringExtra("action"),Toast.LENGTH_SHORT).show();
            return;
        }
        startActivity(cardIntent);
    }

    @Override
    public void onResume(){
        super.onResume();
        // si NFC est activé sur le périphérique, le message NDEF est prêt pour le partage via NFC
        if (nfcAdapter != null && nfcAdapter.isEnabled()) {
            nfcAdapter.setNdefPushMessage(putCardContentToNdefMessage(), this);
        }
        enableNdefExchange();
        whatModeShouldBeOn(cardIntent.getStringExtra("action"));
    }

    // partager une carte avec d'autres appareils Android
    public void shareCard(View view){
        Intent shareCardIntent = getIntent().setClass(this,PartagerActivity.class);
        putCardInfoToIntent();
        startActivity(shareCardIntent);
    }

    // put Card content to NDEF message (for sharing via NFC)
    private NdefMessage putCardContentToNdefMessage(){

        byte[] payload_card_details = cardToJSON(maCarte).getBytes();
        // Pour le moment, il n'y a pas de logo envoyé via la fonctionnalité NFC, donc payload_card_logo est défini sur null
        byte[] payload_card_logo = null;
        if (payload_card_logo == null) payload_card_logo = "null".getBytes();   // si la charge utile du logo est nulle, définir la valeur par défaut du logo

        // créer des enregistrements NDEF et mettre la charge utile de la carte
        NdefRecord record1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,NdefRecord.RTD_TEXT,new byte[0],payload_card_details);
        NdefRecord record2 = NdefRecord.createMime("image/jpeg",payload_card_logo);

        // put NDEF records to NDEF message
        NdefMessage msg = new NdefMessage(new NdefRecord[]{record1, record2, NdefRecord.createApplicationRecord("paulinr.edh.ht.nfccartevisite")});

        Log.d("CardNFC","NDEF message created (" + msg.getRecords().length + " records)");

        return  msg;
    }

    // mettre les informations de la carte dans le fichier JSON(it will be converted to byte[] for sending in NDEF record payload)
    private String cardToJSON(Carte carte){

        try {
            JSONObject cardJSON = new JSONObject();
            cardJSON.put("imgPathLogo",carte.getImgPathLogo());
            cardJSON.put("nom",carte.getNom());
            cardJSON.put("homePhone",carte.getHomePhone());
            cardJSON.put("businessPhone",carte.getBusinessPhone());
            cardJSON.put("email",carte.getEmail());
            cardJSON.put("compagnie",carte.getCompagnie());
            cardJSON.put("addresse",carte.getAddresse());
            cardJSON.put("poste",carte.getPoste());
            cardJSON.put("facebook",carte.getFacebook());
            cardJSON.put("skype",carte.getSkype());
            cardJSON.put("autres",carte.getAutres());

            Log.d("CardNFC","JSON file created!");

            return cardJSON.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    // share Card via Bluetooth
    public void shareViaBluetooth(View view){

        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

        // if adapter was not found bluetooth is not available on the device
        if(btAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available on the device", Toast.LENGTH_LONG).show();
        } else {
            enableBluetooth();
        }
    }

    public void enableBluetooth() {

        // ask for permission to enable Bluetooth and make device visible
        Intent discoveryIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoveryIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, DISCOVER_DURATION);
        startActivityForResult(discoveryIntent, REQUEST_BLU);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode == DISCOVER_DURATION && requestCode == REQUEST_BLU) {

            // set type of file to be sent via Bluetooth
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("text/plain");

            // convert card to json format
            try {

                cardJSON.put("imgPathLogo", "null");           // sending logo with Card via Bluetooth not available
                cardJSON.put("nom", maCarte.getNom());
                cardJSON.put("homePhone", maCarte.getHomePhone());
                cardJSON.put("businessPhone", maCarte.getBusinessPhone());
                cardJSON.put("email",maCarte.getEmail());
                cardJSON.put("compagnie", maCarte.getCompagnie());
                cardJSON.put("addresse",maCarte.getAddresse());
                cardJSON.put("poste",maCarte.getImgPathLogo());
                cardJSON.put("facebook",maCarte.getFacebook());
                cardJSON.put("skype", maCarte.getSkype());
                cardJSON.put("autres", maCarte.getAutres());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // for debugging
            System.out.println("The directory is: " + Environment.getExternalStorageDirectory());

            // write json file to bluetoothCard.json
            try (FileWriter file = new FileWriter("/storage/emulated/0/bluetoothCard.json")) {
                // convert json file to string
                file.write(cardJSON.toString());
                // for debugging
                System.out.println("Nous avons réussi à copier l'objet json dans le fichier ...");
                System.out.println("\nJSON Object: " + cardJSON);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // prepare file to send and send file if bluetooth was found and was not canceled during the process
            File f = new File(Environment.getExternalStorageDirectory(), "bluetoothCard.json");
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
            PackageManager pm = getPackageManager();
            List<ResolveInfo> appsList = pm.queryIntentActivities(intent, 0);

            if(appsList.size() > 0) {
                String packageName = null;
                String className = null;
                boolean found = false;

                for(ResolveInfo info : appsList) {
                    packageName = info.activityInfo.packageName;
                    if(packageName.equals("com.android.bluetooth")) {
                        className = info.activityInfo.name;
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    Toast.makeText(this, "Bluetooth n'a pas été trouvé ...",
                            Toast.LENGTH_LONG).show();
                } else {
                    intent.setClassName(packageName, className);
                    startActivity(intent);
                }
            }
        } else {
            Toast.makeText(this, "Bluetooth a été annulé", Toast.LENGTH_LONG)
                    .show();
        }
    }
    /**
     * Disable foreground dispatcher for handling Intent from NDEF message
     */
    @Override
    public void onPause(){
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }
    /**
     * Handle Intent from NDEF message - forward to CardListActivity
     */
    @Override
    public void onNewIntent (Intent intent){
        if (!mWriteMode && NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            Carte c = getCardFromNdefMessage(intent);
            startActivity(putCardInfoToIntent(c));
        }
        //Tag writing mode
        if (mWriteMode && NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())){
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            writeTag(putCardContentToNdefMessage(),tag);
        }
    }

    /**
     * Method gets Card info from NDEF message
     *
     * @param intent The Intent from NDEF message
     * @return card Card object
     */
    private Carte getCardFromNdefMessage(Intent intent){
        Carte carte = null;
        NdefMessage[] msgs = null;
        Parcelable[] rawMsg = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        if (rawMsg != null){
            msgs = new NdefMessage[rawMsg.length];
            for (int i = 0; i < rawMsg.length; i++){
                msgs[i] = (NdefMessage)rawMsg[i];
            }
        }
        // if there will be 3 NDEF records in NDEF message take payload from first two records (card details and logoImg)
        if (msgs[0].getRecords().length == 3){
            carte = getCardFromJSON(msgs[0].getRecords()[0].getPayload());
            carte.setImgPathLogo(new String(msgs[0].getRecords()[1].getPayload()));
            carte.setId(0);
        }
        // if there will be 2 NDEF records in NDEF message take payload from first record (card details)
        else if (msgs[0].getRecords().length == 2){
           carte = getCardFromJSON(msgs[0].getRecords()[0].getPayload());
            carte.setId(0);
        }
        // if there will less than 2 NDEF records in NDEF message set default values
        else{
            carte = new Carte("null","nom","homePhone","businessPhone","email","compagnie",
                    "addresse","poste","facebook","skype","autres");
            carte.setId(0);
        }

        Log.d("CardNFC","Received NDEF message (" + msgs[0].getRecords().length + " records)");

        return carte;
    }

    /**
     * Method gets Card information from JSON file
     *
     * @param payload_card_details Information in byte[] with Card details
     * @return carte Carte object
     */
    private Carte getCardFromJSON(byte[] payload_card_details){

        Carte carte = null;
        try{
            JSONObject cardJSON = new JSONObject(new String(payload_card_details));
            String imgPathLogo = cardJSON.getString("imgPathLogo");
            String nom = cardJSON.getString("name");
            String homePhone = cardJSON.getString("homePhone");
            String businessPhone = cardJSON.getString("businessPhone");
            String email = cardJSON.getString("email");
            String compagnie = cardJSON.getString("compagnie");
            String addresse = cardJSON.getString("addresse");
            String poste = cardJSON.getString("poste");
            String facebook = cardJSON.getString("facebook");
            String skype = cardJSON.getString("skype");
            String autres = cardJSON.getString("autres");

            carte = new Carte(imgPathLogo,nom,homePhone,businessPhone,email,compagnie,addresse,poste,facebook,skype,autres);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        Log.d("CardNFC","Card from JSON created");
        return carte;
    }
    /**
     * Method puts Card details to Intent
     *
     * @param carte Card object
     * @return intent Intent object witch will be sent to CardsListActivity
     */
    // private method put MyCard data (fields) to Intent object
    private Intent putCardInfoToIntent(Carte carte){
        Intent i = new Intent(this,ListCartesActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("action","newNFC");
        i.putExtra("imgPathLogo","null");  // recevoir le logo via NFC non pris en charge dans cette version de l'application
        i.putExtra("nom",carte.getNom());
        i.putExtra("homePhone",carte.getHomePhone());
        i.putExtra("businessPhone",carte.getBusinessPhone());
        i.putExtra("email",carte.getEmail());
        i.putExtra("compagnie",carte.getCompagnie());
        i.putExtra("addresse",carte.getAddresse());
        i.putExtra("poste",carte.getPoste());
        i.putExtra("facebook",carte.getFacebook());
        i.putExtra("skype",carte.getSkype());
        i.putExtra("autres",carte.getAutres());
        return i;
    }

    // what mode should be on (reading / writing to tag)
    private void whatModeShouldBeOn(String mode){
        if (mode.equals("writeToTag") || mode.equals("myCardWriteToTag")){
            disableNdefExchange();
            enableTagWrite();
            new android.app.AlertDialog.Builder(this).setTitle("Touch TAG to write")
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            disableTagWrite();
                            enableNdefExchange();
                        }
                    }).create().show();
        }
    }




    // write to tag method
    private boolean writeTag(NdefMessage ndefMsg,Tag tag){
        int size = ndefMsg.getByteArrayLength();
        try{
            Ndef ndef = Ndef.get(tag);
            if (ndef != null){
                ndef.connect();
                if (!ndef.isWritable()){
                    Toast.makeText(this,"tag is read-only!",Toast.LENGTH_SHORT).show();
                    return false;
                }
                if (ndef.getMaxSize() < size){
                    Toast.makeText(this,"Tag capacity is " + ndef.getMaxSize() + " bytes, message is " + size + " bytes.",Toast.LENGTH_SHORT).show();
                    return false;
                }
                ndef.writeNdefMessage(ndefMsg);
                Toast.makeText(this,"Wrote message to pre-formatted tag.",Toast.LENGTH_SHORT).show();
                return true;
            }
            else{
                NdefFormatable format = NdefFormatable.get(tag);
                if (format != null){
                    try{
                        format.connect();
                        format.format(ndefMsg);
                        Toast.makeText(this,"Formatted tag and wrote message.",Toast.LENGTH_SHORT).show();
                        return true;
                    }
                    catch (IOException e){
                        Toast.makeText(this,"Failed to format tag.",Toast.LENGTH_SHORT).show();
                        Log.d("NFC",e.getMessage());
                    }
                }
                else {
                    Toast.makeText(this,"Tag doesn't suppoprt NDEF!",Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        }
        catch (Exception e){
            Toast.makeText(this,"Failed to write tag!",Toast.LENGTH_SHORT).show();
            Log.d("NFC",e.getMessage());
        }
        return false;
    }

    // enable NDEF exchange between Android devices
    private void enableNdefExchange(){
        nfcAdapter.enableForegroundDispatch(this,nfcPendingIntent,ndefIntentFilters,null);
        nfcAdapter.setNdefPushMessage(putCardContentToNdefMessage(), this);
    }
    // disable NDEF exchange between Android devices
    private void disableNdefExchange(){
        nfcAdapter.disableForegroundDispatch(this);
    }
    // enable tag writing
    private void enableTagWrite(){
        nfcAdapter.disableForegroundDispatch(this);
        mWriteMode = true;
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        mWriteTagFilters = new IntentFilter[]{tagDetected};
        nfcAdapter.enableForegroundDispatch(this,nfcPendingIntent,mWriteTagFilters,null);
    }
    // disable tag writing
    private void disableTagWrite(){
        mWriteMode = false;
        nfcAdapter.disableForegroundDispatch(this);
    }

}

