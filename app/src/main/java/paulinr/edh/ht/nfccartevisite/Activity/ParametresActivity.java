package paulinr.edh.ht.nfccartevisite.Activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import org.json.JSONObject;

import paulinr.edh.ht.nfccartevisite.Model.Carte;
import paulinr.edh.ht.nfccartevisite.R;



/**
 * Created by Ing Paulinr on 03/03/2018.
 */
public class ParametresActivity extends AppCompatActivity {


    private Button save;
    boolean style1;
    boolean style2;


    private NfcAdapter nfcAdapter;
    private PendingIntent nfcPendingIntent;
    private IntentFilter[] ndefIntentFilters;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametres);
        save = (Button)findViewById(R.id.saveTheme_btn);

        /**
         * NfcAdapter and Intent filter for disabling duplicate app running when receiving Intent from NDEF message
         */
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcPendingIntent = PendingIntent.getActivity(this,0,new Intent(this,getClass()).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);
        IntentFilter ndefFilter = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            ndefFilter.addDataType("text/plain");                   // set payload type which interests us in NDEF message
            ndefIntentFilters = new IntentFilter[]{ndefFilter};
        }
        catch (IntentFilter.MalformedMimeTypeException e) {
            e.printStackTrace();
        }
    }

    public void saveTheme(View view){

        // save style value in internal memory
        SharedPreferences settings = getSharedPreferences("Style", 0);
        SharedPreferences.Editor editor = settings.edit();
        // value style is set when radiobutton is selected
        editor.putBoolean("Style1", style1);
        editor.putBoolean("Style2", style2);

        editor.commit(); // Commit the changes
        Intent showWelcomeActivity = new Intent(this,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(showWelcomeActivity);

    }

    public void onRadioButtonClicked(View view) {
        // check whether the button is currrently checked
        boolean checked = ((RadioButton) view).isChecked();

        // check which radio button was clicked and set bool value that will be used in preferences
        switch(view.getId()) {
            case R.id.radioButton1:
                if (checked)
                    style1 = true;
                    style2 = false;
                break;
            case R.id.radioButton2:
                if (checked)
                    style1 = false;
                    style2 = true;
                break;
            case R.id.radioButton4:
                if (checked)
                    style1 = false;
                    style2 = false;
                break;
        }
    }

    /**
     * Enable foreground dispatcher for handling Intent from NDEF message
     */
    @Override
    public void onResume(){
        super.onResume();
        try {
            nfcAdapter.enableForegroundDispatch(this,nfcPendingIntent,ndefIntentFilters,null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Disable foreground dispatcher for handling Intent from NDEF message
     */
    @Override
    public void onPause(){
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }
    /**
     * Handle Intent from NDEF message - forward to CardListActivity
     */
    @Override
    public void onNewIntent (Intent intent){
        Carte c = getCardFromNdefMessage(intent);
        startActivity(putCardInfoToIntent(c));
    }

    /**
     * Method gets Card info from NDEF message
     *
     * @param intent The Intent from NDEF message
     * @return card Card object
     */
    private Carte getCardFromNdefMessage(Intent intent){
        Carte carte = null;
        NdefMessage[] msgs = null;
        Parcelable[] rawMsg = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        if (rawMsg != null){
            msgs = new NdefMessage[rawMsg.length];
            for (int i = 0; i < rawMsg.length; i++){
                msgs[i] = (NdefMessage)rawMsg[i];
            }
        }
        // s'il y aura 3 enregistrements NDEF dans le message NDEF, prendre la charge utile des deux premiers enregistrements (carte details and logoImg)
        if (msgs[0].getRecords().length == 3){
            carte = getCardFromJSON(msgs[0].getRecords()[0].getPayload());
            carte.setImgPathLogo(new String(msgs[0].getRecords()[1].getPayload()));
            carte.setId(0);
        }
        // s'il y aura 2 enregistrements NDEF dans le message NDEF, prendre la charge utile du premier enregistrement (carte details)
        else if (msgs[0].getRecords().length == 2){
            carte = getCardFromJSON(msgs[0].getRecords()[0].getPayload());
            carte.setId(0);
        }
        // s'il y aura moins de 2 enregistrements NDEF dans les valeurs par défaut du jeu de messages NDEF
        else{
            carte = new Carte("nom","homePhone","cellulaire","email","companie",
                    "addresse","poste","facebook","skype","autres",null);
            carte.setId(0);
        }

        Log.d("CardNFC","Received NDEF message (" + msgs[0].getRecords().length + " records)");

        return carte;
    }

    /**
     * Method gets Card information from JSON file
     *
     * @param payload_card_details Information in byte[] with Card details
     * @return card Card object
     */
    private Carte getCardFromJSON(byte[] payload_card_details){

        Carte carte = null;
        try{
            JSONObject carteJSON = new JSONObject(new String(payload_card_details));

            String nom = carteJSON.getString("nom");
            String homePhone = carteJSON.getString("homePhone");
            String businessPhone = carteJSON.getString("businessPhone");
            String email = carteJSON.getString("email");
            String compagnie = carteJSON.getString("compagnie");
            String addresse = carteJSON.getString("addresse");
            String poste = carteJSON.getString("poste");
            String facebook = carteJSON.getString("facebook");
            String skype = carteJSON.getString("skype");
            String autres = carteJSON.getString("autres");
            String imgPathLogo =carteJSON.getString("imgPathLogo");


            carte = new Carte(nom,homePhone,businessPhone,email,compagnie,addresse,poste,facebook,skype,autres,imgPathLogo);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        Log.d("CardNFC","Card from JSON created");
        return carte;
    }
    /**
     * Method puts Card details to Intent
     *
     * @param carte Card object
     * @return intent Intent object witch will be sent to CardsListActivity
     */
    // private method put MyCard data (fields) to Intent object
    private Intent putCardInfoToIntent(Carte carte){
        Intent i = new Intent(this,ListCartesActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("action","newNFC");
        i.putExtra("imgPathLogo","null");  // receiving Logo via NFC not supported in this app version
        i.putExtra("nom",carte.getNom());
        i.putExtra("homePhone",carte.getHomePhone());
        i.putExtra("BusinessPhone",carte.getBusinessPhone());
        i.putExtra("email",carte.getEmail());
        i.putExtra("compagnie",carte.getCompagnie());
        i.putExtra("addresse",carte.getAddresse());
        i.putExtra("poste",carte.getPoste());
        i.putExtra("facebook",carte.getFacebook());
        i.putExtra("skype",carte.getSkype());
        i.putExtra("autres",carte.getAutres());
        return i;
    }
}
