package paulinr.edh.ht.nfccartevisite.Model;

import android.widget.ImageView;

/**
 * Created by Ing Paulinr on 03/03/2018.
 */
public class Carte {

    // Les champs

    private long id;
    private String nom;
    private String homePhone;
    private String businessPhone;
    private String email;
    private String compagnie;
    private String addresse;
    private String poste;
    private String facebook;
    private String skype;
    private String autres;
    private ImageView logo;
    private String imgPathLogo;


    //Constructeurs

    public Carte(String nom,String homePhone,String businessPhone,String email,String compagnie,String addresse,String poste,String facebook,String skype,String autres,String imgPathLogo) {

        this.nom= nom;
        this.homePhone= homePhone;
        this.businessPhone=businessPhone;
        this.email=email;
        this.compagnie=compagnie;
        this.addresse=addresse;
        this.poste=poste;
        this.facebook=facebook;
        this.skype=skype;
        this.autres=autres;
        this.imgPathLogo=imgPathLogo;

    }

  // Accesseurs et Mutateurs


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompagnie() {
        return compagnie;
    }

    public void setCompagnie(String compagnie) {
        this.compagnie = compagnie;
    }

    public String getAddresse() {
        return addresse;
    }

    public void setAddresse(String addresse) {
        this.addresse = addresse;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getAutres() {
        return autres;
    }

    public void setAutres(String autres) {
        this.autres = autres;
    }

    public ImageView getLogo() {
        return logo;
    }

    public void setLogo(ImageView logo) {
        this.logo = logo;
    }

    public String getImgPathLogo() {
        return imgPathLogo;
    }

    public void setImgPathLogo(String imgPathLogo) {
        this.imgPathLogo = imgPathLogo;
    }

    @Override
    public String toString() {
        return "";
    }
}
