package paulinr.edh.ht.nfccartevisite.Activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.json.JSONObject;

import paulinr.edh.ht.nfccartevisite.Model.Carte;
import paulinr.edh.ht.nfccartevisite.R;

public class MainActivity extends AppCompatActivity {

    private NfcAdapter nfcAdapter;
    private PendingIntent nfcPendingIntent;
    private IntentFilter[] ndefIntentFilters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * l'imageView ici correspond a un image de bienvenue dans l'application
         */
        ImageView img = (ImageView) findViewById(R.id.imageView);

        /**
         *
         * Les "paramètres" de l'objet porteront des informations sur le style
         * de l'application réellement choisi
         */
        SharedPreferences settings = getSharedPreferences("Style", 0);
        /**
         *
         * Valeur booléenne par défaut utilisée pour transporter des informations sur le style1 de l'application
         */
        boolean Style1 = settings.getBoolean("Style1", false);
        /**
         *
         * Valeur booléenne par défaut utilisée pour transporter des informations sur le style2 de l'application
         */
        boolean Style2 = settings.getBoolean("Style2", false);

        /**
         *
         * Définir un logo particulier en fonction du style de l'application sélectionné
         */
        if (Style1 == true)
            img.setImageResource(R.drawable.bienvenue_img4);
        if(Style2==true)
            img.setImageResource(R.drawable.bienvenue_img2);
        /**
         * Filtre NfcAdapter et Intent pour désactiver
         * l'exécution d'une application en double lors de la réception d'un message Intent from NDEF
         */
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter ndefFilter = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            ndefFilter.addDataType("text/plain");     // définir le type de charge utile qui nous intéresse dans le message NDEF
            ndefIntentFilters = new IntentFilter[]{ndefFilter};
        } catch (IntentFilter.MalformedMimeTypeException e) {
            e.printStackTrace();
        }
        // empêcher le lancement deux applications en même temps lors de la réception de message NDEF via NFC
        // Android va ouvrir cette application encore une fois,
        // car le filtre est défini dans Manifest.xml, donc la version actuelle devrait être terminée
        String intentSrc = getIntent().toString();
        // if Intent was sent via NFC (flg=0x10400000) Android will open app again with Main screen (MainActivity)
        if (intentSrc.contains("flg=0x10400000")) {
            Log.d("ParametresCartes", "MainActivity receive Intent from NFC! Activity was finished");
            finish();
        }

    }

    /**
     * Methode qui affiche l'ecran Ma Carte
     * Il crée l'intention avec les détails de "MesCartesActivity.class",
     * y mettre des informations sur l'action effectuée et commencer cette intention.
     *
     * @param view Le bouton sur lequel vous avez cliqué
     */
    public void showCarte(View view){
        Intent showMyCardActivity = new Intent(this,MesCartesActivity.class);
        showMyCardActivity.putExtra("action","myCard");
        startActivity(showMyCardActivity);
    }
    /**
     * Methode qui affiche l'ecran Ma liste de Carte
     *
     * Il crée l'intention avec les détails de "ListCartesActivity.class", y met des informations sur l'action effectuée et commence cette intention.
     *
     * @param view Le bouton sur lequel vous avez cliqué
     */
    public void showCardsList(View view){
        Intent showCardsListActivity = new Intent(this,ListCartesActivity.class);
        showCardsListActivity.putExtra("action","list");
        startActivity(showCardsListActivity);
    }
    /**
     * Méthode qui montre l'écran où la nouvelle carte peut être ajoutée.
     * Il crée l'intention avec les détails de "EditerCarteActivity.class", y met des informations sur l'action effectuée et commence cette intention
     *
     * @param view Le bouton sur lequel vous avez cliqué
     */
    public void showEditScreen (View view){
        Intent showEditScreenActivity = new Intent(this,EditerCarteActivity.class);
        showEditScreenActivity.putExtra("action","new");
        startActivity(showEditScreenActivity);
    }
    /**
     * Méthode qui affiche l'écran des paramètres de style de l'application.
     * Il crée l'intention avec les détails de "PametresActivity.class", y met des informations sur l'action effectuée et commence cette intention
     *
     * @param view Le bouton sur lequel vous avez cliqué
     */
    public void showParameters (View view){
        Intent showSettingsScreenActivity = new Intent(this,ParametresActivity.class);
        showSettingsScreenActivity.putExtra("action","settings");
        startActivity(showSettingsScreenActivity);
    }
    /**
     *La méthode fermera l'application quand on cliquera sur le bouton "TurnOff".
     *
     * @param view Le bouton sur lequel vous avez cliqué
     */
    public void turnOff (View view) {
        finish();
    }

    /**
     * La méthode fermera l'application quand on cliquera sur le bouton "retour".
     */
    @Override
    public void onBackPressed(){
        finish();
    }
    /**
     * Activer le répartiteur de premier plan pour gérer le message Intent from NDEF
     */
    @Override
    public void onResume(){
        super.onResume();
        try {
            nfcAdapter.enableForegroundDispatch(this,nfcPendingIntent,ndefIntentFilters,null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Désactiver le répartiteur de premier plan pour gérer le message Intent from NDEF
     */
    @Override
    public void onPause(){
        super.onPause();
        try {
            nfcAdapter.disableForegroundDispatch(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Gérer l'intention du message NDEF - transmettre à ListCartesActivity
     */
    @Override
    public void onNewIntent (Intent intent){
        Carte c = getCardFromNdefMessage(intent);
        startActivity(putCardInfoToIntent(c));
    }

    /**
     * La méthode obtient l'information de carte du message de NDEF
     *
     * @param intent Le message Intent de NDEF
     * @return carte Carte objet
     */
    private Carte getCardFromNdefMessage(Intent intent){
        Carte carte = null;
        NdefMessage[] msgs = null;
        Parcelable[] rawMsg = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        if (rawMsg != null){
            msgs = new NdefMessage[rawMsg.length];
            for (int i = 0; i < rawMsg.length; i++){
                msgs[i] = (NdefMessage)rawMsg[i];
            }
        }
        // s'il y aura 3 enregistrements NDEF dans le message NDEF, prendre la charge utile des deux premiers enregistrements (carte details and logoImg)
        if (msgs[0].getRecords().length == 3){
            carte = getCardFromJSON(msgs[0].getRecords()[0].getPayload());
            carte.setImgPathLogo(new String(msgs[0].getRecords()[1].getPayload()));
            carte.setId(0);
        }
        // s'il y aura 2 enregistrements NDEF dans le message NDEF, prendre la charge utile du premier enregistrement (carte details)
        else if (msgs[0].getRecords().length == 2){
            carte = getCardFromJSON(msgs[0].getRecords()[0].getPayload());
            carte.setId(0);
        }
        // s'il y aura moins de 2 enregistrements NDEF dans les valeurs par défaut du jeu de messages NDEF
        else{
            carte = new Carte("nom","homePhone","cellulaire","email","companie",
                    "addresse","poste","facebook","skype","autres",null);
            carte.setId(0);
        }

        Log.d("CardNFC","Received NDEF message (" + msgs[0].getRecords().length + " records)");

        return carte;
    }

    /**
     * La méthode obtient les informations de la carte à partir du fichier JSON
     *
     * @param payload_card_details Informations en byte [] avec les détails de la carte
     * @return carte Carte object
     */
    private Carte getCardFromJSON(byte[] payload_card_details){

        Carte carte = null;
        try{
            JSONObject carteJSON = new JSONObject(new String(payload_card_details));

            String nom = carteJSON.getString("nom");
            String homePhone = carteJSON.getString("homePhone");
            String businessPhone = carteJSON.getString("businessPhone");
            String email = carteJSON.getString("email");
            String compagnie = carteJSON.getString("compagnie");
            String addresse = carteJSON.getString("addresse");
            String poste = carteJSON.getString("poste");
            String facebook = carteJSON.getString("facebook");
            String skype = carteJSON.getString("skype");
            String autres = carteJSON.getString("autres");
            String imgPathLogo =carteJSON.getString("imgPathLogo");

            carte = new Carte(nom,homePhone,businessPhone,email,compagnie,addresse,poste,facebook,skype,autres,imgPathLogo);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        Log.d("CardNFC","Card from JSON created");
        return carte;
    }
    /**
     * Method puts Card details to Intent
     *
     * @param carte Carte object
     * @return intent Intent object witch will be sent to CardsListActivity
     */
    // private method put MyCard data (fields) to Intent object
    private Intent putCardInfoToIntent(Carte carte){
        Intent i = new Intent(this,ListCartesActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("action","newNFC");
        i.putExtra("imgPathLogo","null");  // receiving Logo via NFC not supported in this app version
        i.putExtra("nom",carte.getNom());
        i.putExtra("homePhone",carte.getHomePhone());
        i.putExtra("BusinessPhone",carte.getBusinessPhone());
        i.putExtra("email",carte.getEmail());
        i.putExtra("compagnie",carte.getCompagnie());
        i.putExtra("addresse",carte.getAddresse());
        i.putExtra("poste",carte.getPoste());
        i.putExtra("facebook",carte.getFacebook());
        i.putExtra("skype",carte.getSkype());
        i.putExtra("autres",carte.getAutres());
        return i;
    }
    }

