package paulinr.edh.ht.nfccartevisite.Activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import paulinr.edh.ht.nfccartevisite.R;

/**
 * Created by Ing Paulinr on 03/03/2018.
 */
public class EditerCarteActivity extends AppCompatActivity {

    private Button cancel;
    private Button save;
    private Button photo;
    private String logoImgPath;

    private long id;
    private ImageView profile;
    private EditText nom;
    private EditText homePhone;
    private EditText businessPhone;
    private EditText email;
    private EditText compagnie;
    private EditText addresse;
    private EditText poste;
    private EditText facebook;
    private EditText skype;
    private EditText autres;

    //private Card myCard;
    private Intent editCardIntent;

    // for filtering Intents in NDEF message
    private NfcAdapter nfcAdapter;
    private PendingIntent nfcPendingIntent;
    private IntentFilter[] ndefIntentFilters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editer_carte);

        editCardIntent = getIntent();

        id = editCardIntent.getLongExtra("id",0);
        profile = (ImageView)findViewById(R.id.myCard_logo);
        nom = (EditText)findViewById(R.id.myCard_nameVal);
        homePhone = (EditText)findViewById(R.id.myCard_mobileVal);
        businessPhone = (EditText)findViewById(R.id.myCard_phoneVal);
        email = (EditText)findViewById(R.id.myCard_emailVal);
        compagnie = (EditText)findViewById(R.id.myCard_companyVal);
        addresse = (EditText)findViewById(R.id.myCard_addressVal);
        poste = (EditText)findViewById(R.id.myCard_jobVal);
        facebook = (EditText)findViewById(R.id.myCard_facebookVal);
        skype = (EditText)findViewById(R.id.myCard_skypeVal);
        autres = (EditText)findViewById(R.id.myCard_otherVal);

        getCardInfoFromIntent();

        cancel = (Button)findViewById(R.id.editMyCard_cancelBtn);
        save = (Button)findViewById(R.id.editMyCard_saveBtn);
        photo = (Button)findViewById(R.id.takePhotoBtn);

        /**
         * NfcAdapter and Intent filter for disabling duplicate app running when receiving Intent from NDEF message
         */
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcPendingIntent = PendingIntent.getActivity(this,0,new Intent(this,getClass()).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);
        IntentFilter ndefFilter = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            ndefFilter.addDataType("text/plain");                   // set payload type which interests us in NDEF message
            ndefIntentFilters = new IntentFilter[]{ndefFilter};
        }
        catch (IntentFilter.MalformedMimeTypeException e) {
            e.printStackTrace();
        }

    }

    // get card info from Intent object and display on screen
    public void getCardInfoFromIntent(){

        String logoPath = editCardIntent.getStringExtra("logoPath");
        if (logoPath == null || logoPath.equals("null")){
            profile.setImageResource(R.drawable.user_contact);
        }
        else {
            logoImgPath = editCardIntent.getStringExtra("logoPath");
            profile.setImageURI(Uri.parse(logoImgPath));
        }

        nom.setText(editCardIntent.getStringExtra("name"));
        nom.setHint("");
        homePhone.setText(editCardIntent.getStringExtra("mobile"));
        homePhone.setHint("");
        businessPhone.setText(editCardIntent.getStringExtra("phone"));
        businessPhone.setHint("");
        email.setText(editCardIntent.getStringExtra("email"));
        email.setHint("");
        compagnie.setText(editCardIntent.getStringExtra("company"));
        compagnie.setHint("");
        addresse.setText(editCardIntent.getStringExtra("address"));
        addresse.setHint("");
        poste.setText(editCardIntent.getStringExtra("job"));
        poste.setHint("");
        facebook.setText(editCardIntent.getStringExtra("facebook"));
        facebook.setHint("");
        skype.setText(editCardIntent.getStringExtra("skype"));
        skype.setHint("");
        autres.setText(editCardIntent.getStringExtra("other"));
        autres.setHint("");
    }

    // cancel
    public void cancel(View view){
        Toast.makeText(getApplicationContext(),"CANCEL",Toast.LENGTH_SHORT).show();
        finish();
    }
    // save
    public void save(View view){

        if (!isCardReadyToSave()) return;   // check if fields name and/or mobile phone are not empty

        if (editCardIntent.getStringExtra("action").equals("new") || editCardIntent.getStringExtra("action").equals("edit")){
            editCardIntent.setClass(this,ListCartesActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        else if (editCardIntent.getStringExtra("action").equals("newMyCard") || editCardIntent.getStringExtra("action").equals("editMyCard")){
            editCardIntent.setClass(this,MesCartesActivity.class);
        }
        putCardInfoToIntent();
        startActivity(editCardIntent);
    }

    // method checks if name and/or mobile phone are not empty
    private boolean isCardReadyToSave(){
        boolean isReadyToSave = true;
        if (nom.length() == 0) {
            Toast.makeText(getApplicationContext(),"Aucun nom n'a été donné",Toast.LENGTH_SHORT).show();
            isReadyToSave = false;
        }
        if (homePhone.length() == 0 ) {
            Toast.makeText(getApplicationContext(),"Aucun téléphone portable n'est fourni",Toast.LENGTH_SHORT).show();
            isReadyToSave = false;
        }
        return isReadyToSave;
    }


    // take photo
    public void takePhoto (View view){
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePhotoIntent,1);
    }
    // load image from gallery
    public void loadImageFromGallery(View view){
        Intent loadImgIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(loadImgIntent,1);
    }
    // delete logo from MyCard (set default)
    public void deleteLogo(View view){
        profile.setImageResource(R.drawable.user_contact);
        logoImgPath = null;
    }

    // method set logo path from selected image
    @Override
    protected void onActivityResult(int requestCode, int respondCode, Intent data){
        // set taken photo or loaded image to logo ImageView
        if (requestCode == 1 && respondCode == RESULT_OK){
            logoImgPath = data.getData().toString();
            profile.setImageURI(Uri.parse(logoImgPath));
        }
    }

    // private method put MyCard data (fields) to Intent object
    private void putCardInfoToIntent(){
        editCardIntent.putExtra("id",id);
        editCardIntent.putExtra("logoPath",""+logoImgPath);
        editCardIntent.putExtra("name",""+nom.getText());
        editCardIntent.putExtra("mobile",""+homePhone.getText());
        editCardIntent.putExtra("phone",""+businessPhone.getText());
        editCardIntent.putExtra("email",""+email.getText());
        editCardIntent.putExtra("company",""+compagnie.getText());
        editCardIntent.putExtra("address",""+addresse.getText());
        editCardIntent.putExtra("job",""+poste.getText());
        editCardIntent.putExtra("facebook",""+facebook.getText());
        editCardIntent.putExtra("skype",""+skype.getText());
        editCardIntent.putExtra("other",""+autres.getText());
    }

    /**
     * Enable foreground dispatcher for handling Intent from NDEF message
     */
    @Override
    public void onResume(){
        super.onResume();
        try {
            nfcAdapter.enableForegroundDispatch(this,nfcPendingIntent,ndefIntentFilters,null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Disable foreground dispatcher for handling Intent from NDEF message
     */
    @Override
    public void onPause(){
        super.onPause();
        try {
            nfcAdapter.disableForegroundDispatch(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Handle Intent from NDEF message - forward to CardListActivity
     */
    @Override
    public void onNewIntent (Intent intent){
        // do nothing when new Ivent came while editing Card
    }
}
