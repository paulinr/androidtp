package paulinr.edh.ht.nfccartevisite.Activity;

import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import paulinr.edh.ht.nfccartevisite.Adapter.CarteAdapter;
import paulinr.edh.ht.nfccartevisite.Data.DbManager;
import paulinr.edh.ht.nfccartevisite.Model.Carte;
import paulinr.edh.ht.nfccartevisite.R;

/**
 * Created by Ing Paulinr on 03/03/2018.
 */
public class ListCartesActivity extends AppCompatActivity {

    private ListView listCartesView;
    private List<Carte> listeCartes;
    private CarteAdapter cardsAdapter;
    private DbManager dbManager;
    private Intent cardIntent;

    private TextView noCardsTxt;
    private TextView noCardsAddNewTxt;
    private Button newCardBtn;
    private Button bluetoothBtn;

    private BluetoothAdapter btAdapter;

    private NfcAdapter nfcAdapter;
    private PendingIntent nfcPendingIntent;
    private IntentFilter[] ndefIntentFilters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listecartes);

        noCardsTxt = (TextView)findViewById(R.id.txtCarte_on_list);
        noCardsAddNewTxt = (TextView)findViewById(R.id.txtAdd_new);
        newCardBtn = (Button)findViewById(R.id.btn_add_new);
        bluetoothBtn = (Button)findViewById(R.id.btn_read);

        listCartesView = (ListView) findViewById(R.id.lv_listeCartes);
        listeCartes = new ArrayList<Carte>();

        dbManager = new DbManager(getApplicationContext());
        dbManager.open();                   // open connection to database

        cardIntent = getIntent();           // get Intent

        // for NFC sharing
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcPendingIntent = PendingIntent.getActivity(this,0,new Intent(this,getClass()).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);
        IntentFilter ndefFilter = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            ndefFilter.addDataType("text/plain");                   // set payload type which interests us in NDEF message
            ndefIntentFilters = new IntentFilter[]{ndefFilter};
        }
        catch (IntentFilter.MalformedMimeTypeException e) {
            e.printStackTrace();
        }

        serveCardInDatabaseIfNeeded();      // save, edit or delete Card in database (depend of Intent action)
        getCardsFromDatabase();             // get Cards list from database and populate cards list

        cardsAdapter = new CarteAdapter(this, listeCartes);
        listCartesView.setAdapter(cardsAdapter);

        // show Card (go to ShowCardActivity) when user click Card on cards list
        listCartesView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
                Carte carte = listeCartes.get(pos);
                showSelectedCard(carte);
            }
        });

        // add context popup menu to cards list
        registerForContextMenu(listCartesView);
    }

    @Override
    public void onResume(){
        super.onResume();
        nfcAdapter.enableForegroundDispatch(this,nfcPendingIntent,ndefIntentFilters,null);
    }

    @Override
    public void onPause(){
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    public void onNewIntent (Intent intent){
        Log.d("CardNFC","New Intent received");
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())){
            Log.d("CardNFC","NDEF Intent received");
            Carte carte = getCardFromNdefMessage(intent);
            dbManager.insertNewCard("cards",carte);
            getCardsFromDatabase();
            carte.setId(listeCartes.get(listeCartes.size()-1).getId());
            Toast.makeText(getApplicationContext(),"Card SAVED",Toast.LENGTH_SHORT).show();
            showSelectedCard(carte);
        }
    }

    // save new Card, edit Card details or delete Card from database (depending on Intent action)
    private void serveCardInDatabaseIfNeeded(){
        Carte carte = null;
        String action = cardIntent.getAction();
        // if Card details received form other device via NFC
        if (action != null && action.equals(NfcAdapter.ACTION_NDEF_DISCOVERED)) {
            Toast.makeText(this,"NDEF message received!",Toast.LENGTH_SHORT).show();
            carte = getCardFromNdefMessage(cardIntent);          // get Card object from NDEF message
            cardIntent = new Intent();                          // Consume this intent
            cardIntent.putExtra("action","newNFC");             // set Intent action to "new" (new Card received via NFC)
        }
        // if Card details received form this application Activities
        else {
            carte = new Carte(cardIntent.getStringExtra("imgPathLogo"), cardIntent.getStringExtra("nom"), cardIntent.getStringExtra("homePhone"),
                    cardIntent.getStringExtra("businessPhone"), cardIntent.getStringExtra("email"),
                    cardIntent.getStringExtra("compagnie"), cardIntent.getStringExtra("addresse"), cardIntent.getStringExtra("poste"),
                    cardIntent.getStringExtra("facebook"), cardIntent.getStringExtra("skype"),
                    cardIntent.getStringExtra("autres"));
            carte.setId(cardIntent.getLongExtra("id", 0));
        }
        // add new Card to database
        if (cardIntent.getStringExtra("action").equals("new")){
            dbManager.insertNewCard("cards",carte);
            Toast.makeText(getApplicationContext(),"Card SAVED",Toast.LENGTH_SHORT).show();
        }
        // when new Card received via NFC - add new Card to database and show it (go to ShowCardActivity)
        if (cardIntent.getStringExtra("action").equals("newNFC")){
            dbManager.insertNewCard("cards",carte);
            getCardsFromDatabase();
            carte.setId(listeCartes.get(listeCartes.size()-1).getId());
            Toast.makeText(getApplicationContext(),"Carte enregistrée",Toast.LENGTH_SHORT).show();
            showSelectedCard(carte);
        }
        // edit Card details in database
        else if (cardIntent.getStringExtra("action").equals("edit")){
            dbManager.updateCard("cards",carte);
            Toast.makeText(getApplicationContext(),"Carte editee",Toast.LENGTH_SHORT).show();
        }
        // delete Card from database
        else if (cardIntent.getStringExtra("action").equals("delete")){
            dbManager.deleteCard("cards",carte.getId());
            Toast.makeText(getApplicationContext(),"Carte supprimée!",Toast.LENGTH_SHORT).show();
        }
    }

    // method gets all Cards from database and populate cards list
    private void getCardsFromDatabase(){
        Cursor cardCursor = dbManager.getAllCardsFromDB("cards");

        if (cardCursor != null && cardCursor.getCount() > 0) {
            cardCursor.moveToFirst();
            do{
                long id = cardCursor.getLong(0);
                String name = cardCursor.getString(1);
                String mobile = cardCursor.getString(2);
                String phone = cardCursor.getString(3);
                String email = cardCursor.getString(4);
                String company = cardCursor.getString(5);
                String address = cardCursor.getString(6);
                String job = cardCursor.getString(7);
                String facebook = cardCursor.getString(8);
                String skype = cardCursor.getString(9);
                String autres = cardCursor.getString(10);
                String logoPath = cardCursor.getString(11);

                Carte carte = new Carte(name,mobile,phone,email,company,address,job,facebook,skype,autres,logoPath);
                carte.setId(id);
                listeCartes.add(carte);
            }
            while (cardCursor.moveToNext());
        }
        else {
            showNoCardsInfoIfListEmpty();       // if no cards element in database display "no cards" info and show "add new" button
        }

    }

    // show selected Card (go to ShowCardActivity)
    private void showSelectedCard(Carte carte){
        Intent showCardIntent = new Intent(this,AfficherCarteActivity.class);
        showCardIntent.putExtra("action","cardFromList");
        showCardIntent.putExtra("id",carte.getId());
        showCardIntent.putExtra("imgPathLogo",""+carte.getImgPathLogo());
        showCardIntent.putExtra("nom",""+carte.getNom());
        showCardIntent.putExtra("homePhone",""+carte.getHomePhone());
        showCardIntent.putExtra("businessPhone",""+carte.getBusinessPhone());
        showCardIntent.putExtra("email",""+carte.getEmail());
        showCardIntent.putExtra("compagnie",""+carte.getCompagnie());
        showCardIntent.putExtra("addresse",""+carte.getAddresse());
        showCardIntent.putExtra("poste",""+carte.getPoste());
        showCardIntent.putExtra("facebook",""+carte.getFacebook());
        showCardIntent.putExtra("skype",""+carte.getSkype());
        showCardIntent.putExtra("autres",""+carte.getAutres());
        startActivity(showCardIntent);
    }
    // edit selected Card (go to EditCardActivity)
    private void editSelectedCard(Carte carte){
        Intent editCardIntent = new Intent(this,EditerCarteActivity.class);
        editCardIntent.putExtra("action","edit");
        editCardIntent.putExtra("id",carte.getId());
        editCardIntent.putExtra("imgPathLogo",""+carte.getImgPathLogo());
        editCardIntent.putExtra("nom",""+carte.getNom());
        editCardIntent.putExtra("homePhone",""+carte.getHomePhone());
        editCardIntent.putExtra("businessPhone",""+carte.getBusinessPhone());
        editCardIntent.putExtra("email",""+carte.getEmail());
        editCardIntent.putExtra("compagnie",""+carte.getCompagnie());
        editCardIntent.putExtra("addresse",""+carte.getAddresse());
        editCardIntent.putExtra("poste",""+carte.getPoste());
        editCardIntent.putExtra("facebook",""+carte.getFacebook());
        editCardIntent.putExtra("skype",""+carte.getSkype());
        editCardIntent.putExtra("autres",""+carte.getAutres());
        startActivity(editCardIntent);
    }

    // method displays "no cards" info and shows "add new" button
    private void showNoCardsInfoIfListEmpty(){
        noCardsTxt.setVisibility(View.VISIBLE); ;
        noCardsAddNewTxt.setVisibility(View.VISIBLE);
        newCardBtn.setVisibility(View.VISIBLE);
    }


    // Close connection to database when Activity is closed - onDestroy()
    @Override
    public void onDestroy(){
        if (dbManager != null) dbManager.close();   // close connection to database
        super.onDestroy();
    }

    // create a popup menu, it will be displayed when user long press Card item on a cards list
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.popup_menu, menu);

    }

    // what to do when user click one of popup menu items
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final int i = (int)info.id;
        long id = listeCartes.get(i).getId();

        switch (item.getItemId()) {
            // show selected Card (go to ShowCardActivity)
            case R.id.menu_show:
                showSelectedCard(listeCartes.get(i));
                return true;
            // edit selected Card (go to EditCardActivity)
            case R.id.menu_edit:
                editSelectedCard(listeCartes.get(i));
                return true;
            // delete selected Card
            case R.id.menu_delete:
                new AlertDialog.Builder(this).setTitle("Supprimer la carte de visite?")
                        .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int j) {
                                dbManager.deleteCard("cards",listeCartes.get(i).getId());
                                listeCartes.remove(i);
                                listCartesView.setAdapter(cardsAdapter);
                                Toast.makeText(getApplicationContext(),"Card deleted!",Toast.LENGTH_SHORT).show();
                                if(listeCartes.size() == 0) showNoCardsInfoIfListEmpty();
                                return;
                            }
                        })
                        .setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int j) {
                                return;
                            }
                        })
                        .show();
            default:
                return super.onContextItemSelected(item);
        }
    }

    // create new Card if user clicked (+) - go to EditCardActivity
    public void createNewCard(View view){
        cardIntent = new Intent(this,EditerCarteActivity.class);
        cardIntent.putExtra("action","new");
        startActivity(cardIntent);
    }

    // when "back" button is pressed go back to MainActivity (Main screen)
    @Override
    public void onBackPressed(){
        startActivity(new Intent(this,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        finish();
    }

    // get Card info from NDEF message
    private Carte getCardFromNdefMessage(Intent intent){
        Carte carte = null;
        NdefMessage[] msgs = null;
        Parcelable[] rawMsg = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        if (rawMsg != null){
            msgs = new NdefMessage[rawMsg.length];
            for (int i = 0; i < rawMsg.length; i++){
                msgs[i] = (NdefMessage)rawMsg[i];
            }
        }
        // if there will be 3 NDEF records in NDEF message take payload from first two records (card details and logoImg)
        if (msgs[0].getRecords().length == 3){
            carte = getCardFromJSON(msgs[0].getRecords()[0].getPayload());
            carte.setImgPathLogo(new String(msgs[0].getRecords()[1].getPayload()));
            carte.setId(0);
        }
        // if there will be 2 NDEF records in NDEF message take payload from first record (card details)
        else if (msgs[0].getRecords().length == 2){
            carte = getCardFromJSON(msgs[0].getRecords()[0].getPayload());
            carte.setId(0);
        }
        // if there will less than 2 NDEF records in NDEF message set default values
        else{
            carte = new Carte("nom","homePhone","businessPhone","email","companie",
                    "addresse","poste","facebook","skype","autres",null);
            carte.setId(0);
        }

        Log.d("CardNFC","Received NDEF message (" + msgs[0].getRecords().length + " records)");

        return carte;
    }

    /**
     * La méthode obtient les informations de la carte à partir du fichier JSON
     *
     * @param payload_card_details Informations en byte [] avec les détails de la carte
     * @return carte Carte object
     */
    private Carte getCardFromJSON(byte[] payload_card_details){

        Carte carte = null;
        try{
            JSONObject carteJSON = new JSONObject(new String(payload_card_details));

            String nom = carteJSON.getString("nom");
            String homePhone = carteJSON.getString("homePhone");
            String businessPhone = carteJSON.getString("businessPhone");
            String email = carteJSON.getString("email");
            String compagnie = carteJSON.getString("compagnie");
            String addresse = carteJSON.getString("addresse");
            String poste = carteJSON.getString("poste");
            String facebook = carteJSON.getString("facebook");
            String skype = carteJSON.getString("skype");
            String autres = carteJSON.getString("autres");
            String imgPathLogo =carteJSON.getString("imgPathLogo");

            carte = new Carte(nom,homePhone,businessPhone,email,compagnie,addresse,poste,facebook,skype,autres,imgPathLogo);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        Log.d("CardNFC","Card from JSON created");
        return carte;
    }

    // read json file send via Bluetooth from other device and save to database
    public void readFromBluetoothFile(View view)  {
        // find location of json file in phone memory, if file was not sent/saved communicate this to the user
        File f = null;
        try {
            File jsonLocation = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            f = new File(jsonLocation,"bluetoothCard.json");
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),"File is not in download directory, please re-send",Toast.LENGTH_SHORT).show();
        }

        //info for debugging if file exists
        if (f.exists()) {
            System.out.println("it exists");
        }

        // convert jason file to byte array
        byte[] bytesArray = new byte[(int) f.length()];
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            //read file into bytes[]
            fis.read(bytesArray);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //      String action = cardIntent.getAction();

        // create card from bytes array pulled from .json file and save it to database
        dbManager.insertNewCard("cards",getCardFromJSON(bytesArray));
        // inform user that card was correctly saved
        Toast.makeText(getApplicationContext(),"Card SAVED",Toast.LENGTH_SHORT).show();
        // delete file received by bluetooth (no longer needed)
        f.delete();
        Toast.makeText(getApplicationContext(),"Bluetooth file deleted",Toast.LENGTH_SHORT).show();
    }



}
