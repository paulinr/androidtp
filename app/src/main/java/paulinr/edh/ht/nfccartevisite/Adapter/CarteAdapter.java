package paulinr.edh.ht.nfccartevisite.Adapter;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import paulinr.edh.ht.nfccartevisite.Model.Carte;
import paulinr.edh.ht.nfccartevisite.R;

/**
 * Created by Ing Paulinr on 03/03/2018.
 */
public class CarteAdapter extends ArrayAdapter<Carte> {

    private Activity context;
    private List<Carte> listeCartes;

    public CarteAdapter(Activity context, List<Carte> listeCartes){

        super(context, R.layout.liste_cartes_item,listeCartes);
        this.context=context;
        this.listeCartes=listeCartes;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater=context.getLayoutInflater();
        View rowView =layoutInflater.inflate(R.layout.liste_cartes_item,null,true);

        TextView nom = (TextView) rowView.findViewById(R.id.txtNom);
        TextView homePhone = (TextView) rowView.findViewById(R.id.txtHomephone);
        TextView cellulaire = (TextView) rowView.findViewById(R.id.txtCellular);
        ImageView profile = (ImageView) rowView.findViewById(R.id.logo_profile);

        nom.setText(listeCartes.get(position).getNom());
        homePhone.setText(listeCartes.get(position).getHomePhone());
        cellulaire.setText(listeCartes.get(position).getBusinessPhone());

        String logoProfile = listeCartes.get(position).getImgPathLogo();
        if (!logoProfile.equals("null")) {
            profile.setImageURI(Uri.parse(logoProfile));
        }
        else{
           profile.setImageResource(R.drawable.user_contact);
        }


        return rowView ;
    }
}
