package paulinr.edh.ht.nfccartevisite.Activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONObject;

import paulinr.edh.ht.nfccartevisite.Model.Carte;
import paulinr.edh.ht.nfccartevisite.R;

/**
 * Created by Ing Paulinr on 03/03/2018.
 */
public class PartagerActivity extends AppCompatActivity{

    private Intent intent;
    private NfcAdapter nfcAdapter;
    private PendingIntent nfcPendingIntent;
    private IntentFilter[] ndefIntentFilters;

    private static boolean closeShareActivity = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partager_carte);
        setTitle("Partager via :");

        /**
         * NfcAdapter and Intent filter for disabling duplicate app running when receiving Intent from NDEF message
         */
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcPendingIntent = PendingIntent.getActivity(this,0,new Intent(this,getClass()).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);
        IntentFilter ndefFilter = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            ndefFilter.addDataType("text/plain");                   // set payload type which interests us in NDEF message
            ndefIntentFilters = new IntentFilter[]{ndefFilter};
        }
        catch (IntentFilter.MalformedMimeTypeException e) {
            e.printStackTrace();
        }
    }


    // method to share Card via NFC
    public void shareViaNfc (View view){
        checkIfNfcAvailable();              // check is NFC available and show Toast message
    }


    // method will close this Activity when back from NFC Settings and NFC was turn-on by user
    private void closeThisActivityIfDontNeed(){
        if (nfcAdapter.isEnabled() && closeShareActivity){
            closeShareActivity = false;     // don't close this Activity next time
            Toast.makeText(this,"NFC is on.",Toast.LENGTH_LONG).show();
            finish();
        }
        closeShareActivity = false;
    }

    /**
     * Enable foreground dispatcher for handling Intent from NDEF message
     */
    @Override
    public void onResume(){
        super.onResume();
        closeThisActivityIfDontNeed();     // close this Activity when user come back from Settings and turn-on NFC
        nfcAdapter.enableForegroundDispatch(this,nfcPendingIntent,ndefIntentFilters,null);
    }
    /**
     * Disable foreground dispatcher for handling Intent from NDEF message
     */
    @Override
    public void onPause(){
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }
    /**
     * Handle Intent from NDEF message - forward to CardListActivity
     */
    @Override
    public void onNewIntent (Intent intent){
        Carte c = getCardFromNdefMessage(intent);
        startActivity(putCardIntoIntent(c));
    }

    /**
     * Method gets Card info from NDEF message
     *
     * @param intent The Intent from NDEF message
     * @return card Card object
     */
    private Carte getCardFromNdefMessage(Intent intent){
        Carte carte = null;
        NdefMessage[] msgs = null;
        Parcelable[] rawMsg = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        if (rawMsg != null){
            msgs = new NdefMessage[rawMsg.length];
            for (int i = 0; i < rawMsg.length; i++){
                msgs[i] = (NdefMessage)rawMsg[i];
            }
        }
        // if there will be 3 NDEF records in NDEF message take payload from first two records (card details and logoImg)
        if (msgs[0].getRecords().length == 3){
            carte = getCardFromJSON(msgs[0].getRecords()[0].getPayload());
            carte.setImgPathLogo(new String(msgs[0].getRecords()[1].getPayload()));
            carte.setId(0);
        }
        // if there will be 2 NDEF records in NDEF message take payload from first record (card details)
        else if (msgs[0].getRecords().length == 2){
            carte = getCardFromJSON(msgs[0].getRecords()[0].getPayload());
            carte.setId(0);
        }
        // if there will less than 2 NDEF records in NDEF message set default values
        else{
            carte = new Carte("card_name","card_mobile","card_phone","card_email","card_company",
                    "card_address","card_job","card_facebook","card_skype","card_other","null");
            carte.setId(0);
        }

        Log.d("CardNFC","Received NDEF message (" + msgs[0].getRecords().length + " records)");

        return carte;
    }

    /**
     * Method gets Card information from JSON file
     *
     * @param payload_card_details Information in byte[] with Card details
     * @return card Card object
     */
    private Carte getCardFromJSON(byte[] payload_card_details){

        Carte carte = null;
        try{
            JSONObject cardJSON = new JSONObject(new String(payload_card_details));

            String nom = cardJSON.getString("nom");
            String homePhone = cardJSON.getString("homePhone");
            String businessPhone = cardJSON.getString("businessPhone");
            String email = cardJSON.getString("email");
            String compagnie = cardJSON.getString("compagnie");
            String addresse = cardJSON.getString("addresse");
            String poste = cardJSON.getString("poste");
            String facebook = cardJSON.getString("facebook");
            String skype = cardJSON.getString("skype");
            String autres = cardJSON.getString("autres");
            String imgPathLogo = cardJSON.getString("imgPathLogo");

            carte = new Carte(nom,homePhone,businessPhone,email,compagnie,addresse,poste,facebook,skype,autres,imgPathLogo);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        Log.d("CardNFC","Card from JSON created");
        return carte;
    }
    /**
     * Method puts Card details to Intent
     *
     * @param carte Carte object
     * @return intent Intent object witch will be sent to CardsListActivity
     */
    // private method put Card data (fields) to Intent object
    private Intent putCardIntoIntent(Carte carte){
        Intent i = new Intent(this,AfficherCarteActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("action","newNFC");

        i.putExtra("nom",carte.getNom());
        i.putExtra("mobile",carte.getHomePhone());
        i.putExtra("businessPhone",carte.getBusinessPhone());
        i.putExtra("email",carte.getEmail());
        i.putExtra("compagnie",carte.getCompagnie());
        i.putExtra("addresse",carte.getAddresse());
        i.putExtra("poste",carte.getPoste());
        i.putExtra("facebook",carte.getFacebook());
        i.putExtra("skype",carte.getSkype());
        i.putExtra("autres",carte.getAutres());
        i.putExtra("imgPathLogo","null");  // receiving Card with logo via NFC not supported in this app version
        return i;
    }

    // private method gets Card from Intent
    private Carte getCardFromIntent() {
        Intent intent = getIntent();
        Carte carte = new Carte(intent.getStringExtra("nom"), intent.getStringExtra("homePhone"), intent.getStringExtra("businessPhone"),
                intent.getStringExtra("email"),
                intent.getStringExtra("compagnie"), intent.getStringExtra("addresse"), intent.getStringExtra("poste"),
                intent.getStringExtra("facebook"), intent.getStringExtra("skype"),
                intent.getStringExtra("autres"),intent.getStringExtra("imgPathLogo"));
        carte.setId(intent.getLongExtra("id", 0));
        return carte;
    }

    // write Card to NFC tag - go to ShowCardActivity and write Card to tag
    public void writeToNfcTag(View view){
        boolean shouldWriteTag = checkIfNfcAvailable();
        if (shouldWriteTag) {
            String action = getIntent().getStringExtra("action");
            Intent writeToTag = putCardIntoIntent(getCardFromIntent()).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            if (action.equals("myCard") || action.equals("editMyCard")) writeToTag.putExtra("action","myCardWriteToTag");
            else writeToTag.putExtra("action","writeToTag");
            startActivity(writeToTag);
            finish();
        }
    }

    private boolean checkIfNfcAvailable(){
        // if device not support NFC - display information about this to the user
        if (nfcAdapter == null) {
            Toast.makeText(this,"NFC not supported at your device!",Toast.LENGTH_SHORT).show();
            finish();
            return false;
        }
        // if device support NFC but it's disable at the moment - go to NFC Settings
        else if (!nfcAdapter.isEnabled()){
            Toast.makeText(this,"Turn on NFC.",Toast.LENGTH_LONG).show();
            closeShareActivity = true;          // user goes to NFC Settings, Activity can be close if come back after turning-on NFC
            Intent gotoNfcSettings = new Intent(Settings.ACTION_NFC_SETTINGS);
            startActivity(gotoNfcSettings);
            return false;
        }
        // if user choose to share Card via NFC / write to tag while NFC is on - display that NFC is on
        else {
            Toast.makeText(this,"NFC is on.",Toast.LENGTH_SHORT).show();
            finish();
            return true;
        }
    }
}
