package paulinr.edh.ht.nfccartevisite.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import paulinr.edh.ht.nfccartevisite.Data.DbManager;
import paulinr.edh.ht.nfccartevisite.Model.Carte;
import paulinr.edh.ht.nfccartevisite.R;

/**
 * Created by Ing Paulinr on 03/03/2018.
 */
public class MesCartesActivity extends AppCompatActivity {

    private DbManager dbManager;
    private Intent myCardIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mes_cartes);

        myCardIntent = getIntent();

        dbManager = new DbManager(getApplicationContext());
        dbManager.open();                   // open connection to database

        serveMyCardInDatabaseIfNeeded();     // save, edit or delete myCard from database if user select specific action
        showMyCardIfExists();               // checks if there is myCard element in database, if yes it shows it (goes to ShowCardActivity)
    }

    // create new myCard (go to EditCardActivity)
    public void createNewMyCard(View view){
        myCardIntent = new Intent(this,EditerCarteActivity.class);
        myCardIntent.putExtra("action","newMyCard");
        startActivity(myCardIntent);
    }

    // method checks if there is myCard element in database, if yes it shows myCard (goes to ShowCardActivity)
    private void showMyCardIfExists(){
        Cursor cardCursor = dbManager.getAllCardsFromDB("mycards");
        //if (cardCursor != null) Toast.makeText(getApplicationContext(),"Records in DB  = " + cardCursor.getCount(),Toast.LENGTH_SHORT).show();
        if (cardCursor != null && cardCursor.getCount() == 1){
            cardCursor.moveToFirst();

            Intent myCardIntent = new Intent(this,AfficherCarteActivity.class);
            myCardIntent.putExtra("action","myCard");
            myCardIntent.putExtra("id",cardCursor.getLong(0));
            myCardIntent.putExtra("nom",""+cardCursor.getString(1));
            myCardIntent.putExtra("homePhone",""+cardCursor.getString(2));
            myCardIntent.putExtra("businessPhone",""+cardCursor.getString(3));
            myCardIntent.putExtra("email",""+cardCursor.getString(4));
            myCardIntent.putExtra("compagnie",""+cardCursor.getString(5));
            myCardIntent.putExtra("addresse",""+cardCursor.getString(6));
            myCardIntent.putExtra("poste",""+cardCursor.getString(7));
            myCardIntent.putExtra("facebook",""+cardCursor.getString(8));
            myCardIntent.putExtra("skype",""+cardCursor.getString(9));
            myCardIntent.putExtra("autres",""+cardCursor.getString(10));
            myCardIntent.putExtra("imgPathLogo",""+cardCursor.getString(11));
            startActivity(myCardIntent);
        }
    }

    // method saves myCard into database if there is need (Intent from EditCardActivity)
    private void serveMyCardInDatabaseIfNeeded(){
        myCardIntent = getIntent();

        Carte carte = new Carte(myCardIntent.getStringExtra("nom"),myCardIntent.getStringExtra("homePhone"),
                myCardIntent.getStringExtra("businessPhone"),myCardIntent.getStringExtra("email"),
                myCardIntent.getStringExtra("compagnie"),myCardIntent.getStringExtra("addresse"),myCardIntent.getStringExtra("poste"),
                myCardIntent.getStringExtra("facebook"),myCardIntent.getStringExtra("skype"),
                myCardIntent.getStringExtra("autres"),myCardIntent.getStringExtra("imgPathLogo"));
        carte.setId(myCardIntent.getLongExtra("id",0));
        // add new Card to database
        if (myCardIntent.getStringExtra("action").equals("newMyCard")){
            dbManager.insertNewCard("mycards",carte);
            Toast.makeText(getApplicationContext(),"Card SAVED",Toast.LENGTH_SHORT).show();
        }
        // edit Card details in database
        else if (myCardIntent.getStringExtra("action").equals("editMyCard")){
            dbManager.updateCard("mycards",carte);
            Toast.makeText(getApplicationContext(),"Card edited",Toast.LENGTH_SHORT).show();
        }
        // delete Card from database
        else if (myCardIntent.getStringExtra("action").equals("delete")){
            dbManager.deleteCard("mycards",carte.getId());
            Toast.makeText(getApplicationContext(),"Card deleted!",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroy(){
        if (dbManager != null) dbManager.close();   // close connection to database
        super.onDestroy();
    }

    // when "back" button is pressed go back to WelcomeActivity (Main screen)
    @Override
    public void onBackPressed(){
        startActivity(new Intent(this,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }
}
